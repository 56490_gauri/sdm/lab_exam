const express = require('express')
const cors = require('cors')
const routerMovie = require('./routes/movie')

const app = express()

app.use(cors('*'))
app.use(express.json())

app.use('/movie',routerMovie)

app.listen(4000,()=>
{console.log('server started on port 4000')}
)