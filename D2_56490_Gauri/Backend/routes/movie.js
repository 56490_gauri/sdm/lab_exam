const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

router.post('/addMovie', (request, response) => {
    const { movieTitle, movieDate, movieTime, directorName} = request.body
  
    const statement = `
      insert into movie
        (movie_title, movie_release_date,movie_time,director_name)
      values 
        ('${movieTitle}', '${movieDate}', '${movieTime}','${directorName}')
    `  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/displayMovie', (request, response) => {
    const { movieId } = request.body
    const statement = `
      select * from movie where movie_id = '${movieId}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/updateMovie', (request, response) => {
    const {movieId, movieDate, movieTime } = request.body
    const statement = `
      update movie set movie_release_date ='${movieDate}',movie_time = '${movieTime}' where movie_id = '${movieId}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })

  router.post('/deleteMovie', (request, response) => {
    const {movieId} = request.body
    const statement = `
      delete from movie where movie_id  = '${movieId}'`
  
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
      connection.end()
      response.send(utils.createResult(error, result))
    })
  })



module.exports = router